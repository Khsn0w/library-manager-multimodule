package tn.insat.libraryapi.controllers;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tn.insat.libraryapi.security.JwtTokenProvider;
import tn.insat.libraryapi.utils.exceptions.AppException;
import tn.insat.libraryapi.viewmodels.auth.ApiResponse;
import tn.insat.libraryapi.viewmodels.auth.JwtAuthenticationResponse;
import tn.insat.libraryapi.viewmodels.auth.LoginRequest;
import tn.insat.libraryapi.viewmodels.auth.SignUpRequest;
import tn.insat.persistance.Repositories.IClientRepository;
import tn.insat.persistance.Repositories.IRoleRepository;
import tn.insat.persistance.models.Client;
import tn.insat.persistance.models.Role;
import tn.insat.persistance.models.RoleName;


import javax.validation.Valid;
import java.util.Collections;

@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    IClientRepository clientRepository;

    @Autowired
    IRoleRepository roleRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    JwtTokenProvider tokenProvider;

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsernameOrEmail(),
                        loginRequest.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = tokenProvider.generateToken(authentication);
        return ResponseEntity.ok(new JwtAuthenticationResponse(jwt));
    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpRequest signUpRequest) {
        if(clientRepository.findByEmail(signUpRequest.getEmail()).isPresent()) {
            return new ResponseEntity(new ApiResponse(false, "Email Already Taken!"),
                    HttpStatus.BAD_REQUEST);
        }


        // Creating user's account
        Client client = new Client(signUpRequest.getEmail(), signUpRequest.getName(), signUpRequest.getUsername() , signUpRequest.getPassword());

        client.setPassword(passwordEncoder.encode(client.getPassword()));

        Role userRole = roleRepository.findByName(RoleName.ROLE_USER)
                .orElseThrow(() -> new AppException("User Role not set."));

        client.setRoles(Collections.singleton(userRole));

        Client result = clientRepository.save(client);

        return new ResponseEntity("User Created",HttpStatus.CREATED);
    }
}