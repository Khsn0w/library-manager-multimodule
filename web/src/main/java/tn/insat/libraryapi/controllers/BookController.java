package tn.insat.libraryapi.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tn.insat.persistance.exception.BookNotFoundException;
import tn.insat.persistance.models.Book;
import tn.insat.persistance.services.Abstract.IBookService;

import java.util.List;

/**
 * Controller To manage Book Entity
 */
@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/book")
public class BookController {
    @Autowired
    IBookService bookService;

    @PostMapping
    public ResponseEntity storeBook(@RequestBody Book book) {

        bookService.create(book);
        return new ResponseEntity(book, HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity getAll() {
        List<Book> books = bookService.getAll();
        return new ResponseEntity(books, HttpStatus.OK);
    }

    @GetMapping(params = {"id"})
    public ResponseEntity getById(@RequestParam("id") long id) {
        Book book = null;
        try {
            book = bookService.findById(id);
            return new ResponseEntity(book, HttpStatus.OK);
        } catch (BookNotFoundException exception) {
            return new ResponseEntity(exception.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping
    public ResponseEntity updateBook(@RequestBody Book book) {
        try {
            bookService.update(book);
            return new ResponseEntity(book, HttpStatus.ACCEPTED);
        } catch (Exception exception) {
            return new ResponseEntity(exception.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping
    public ResponseEntity deleteBook(@RequestBody Book book) {
        try {
            bookService.delete(book);
            return new ResponseEntity(book, HttpStatus.ACCEPTED);
        } catch (BookNotFoundException exception) {
            return new ResponseEntity(exception.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping(params = {"id"})
    public ResponseEntity deleteBookById(@RequestParam("id") long id) {
        try {
            bookService.deleteById(id);
            return new ResponseEntity("Deleted ", HttpStatus.ACCEPTED);
        } catch (BookNotFoundException exception) {
            return new ResponseEntity(exception.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }


}
