package tn.insat.libraryapi.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tn.insat.persistance.exception.ClientNotFoundException;
import tn.insat.persistance.models.Command;
import tn.insat.persistance.services.Abstract.IBuyService;
import tn.insat.persistance.services.Abstract.ICommandService;
import tn.insat.persistance.viewmodels.BuyServiceRequest;

import java.util.List;

@RestController
@RequestMapping("/Command")
public class CommandController {
    @Autowired
    ICommandService commandService;

    @Autowired
    IBuyService buyService;

    @GetMapping
    public ResponseEntity getCommands(@RequestParam("id") long id) {
        List<Command> commands = commandService.findByClient(id);

        return new ResponseEntity(commands, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity storeCommand(@RequestBody BuyServiceRequest request) {
        try {
            buyService.buyBooks(request);
            return new ResponseEntity("Created" , HttpStatus.CREATED);
        }catch(ClientNotFoundException exceptions){
            return new ResponseEntity("Client not found" , HttpStatus.BAD_REQUEST);
        }
    }


}
