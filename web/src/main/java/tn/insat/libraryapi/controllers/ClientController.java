package tn.insat.libraryapi.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tn.insat.persistance.exception.ClientExistException;
import tn.insat.persistance.exception.ClientNotFoundException;
import tn.insat.persistance.models.Client;
import tn.insat.persistance.services.Abstract.IClientService;

import java.util.List;

@RestController
@RequestMapping(value = "/client")
public class ClientController {
    @Autowired
    private IClientService clientService;

    @GetMapping
    public ResponseEntity<List<Client>> getClients() {
        List<Client> clients = clientService.getAll();
        return new ResponseEntity<List<Client>>(clients, HttpStatus.OK);
    }


    @GetMapping(params = {"id"})
    public ResponseEntity<Client> getClient(@RequestParam("id") long id) {
        Client client = null;
        try{
            client = clientService.findById(id);
            return new ResponseEntity(client, HttpStatus.OK);
        }catch(ClientNotFoundException exception){
            return new ResponseEntity("Client not found", HttpStatus.NOT_FOUND);
        }

    }


    @PostMapping
    public ResponseEntity store(@RequestBody Client client) {
        try {
            clientService.create(client);
            return new ResponseEntity<Client>(client, HttpStatus.CREATED);
        } catch (ClientExistException exception) {
            return new ResponseEntity("Client Already Exist", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping
    public ResponseEntity update(@RequestBody Client client){
        try {
            clientService.update(client);
            return new ResponseEntity<Client>(client, HttpStatus.ACCEPTED);
        } catch (ClientNotFoundException exception) {
            return new ResponseEntity("Client Not Exist", HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping
    public ResponseEntity delete(@RequestBody Client client){
        try {
            clientService.delete(client);
            return new ResponseEntity<Client>(client, HttpStatus.ACCEPTED);
        } catch (ClientNotFoundException exception) {
            return new ResponseEntity("Client Not Exist", HttpStatus.BAD_REQUEST);
        }
    }


    @DeleteMapping(params = {"id"})
    public ResponseEntity deleteById(@RequestParam("id") long id){
        try {
            clientService.deleteById(id);
            return new ResponseEntity("Deleted", HttpStatus.NO_CONTENT);
        } catch (ClientNotFoundException exception) {
            return new ResponseEntity("Client Not Exist", HttpStatus.BAD_REQUEST);
        }
    }
}
