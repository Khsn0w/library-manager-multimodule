package tn.insat.persistance.exception;

public class ClientNotFoundException extends Exception{

    private String email;
    private Long id;
    public ClientNotFoundException(String email) {
        this.email = email;
    }

    public ClientNotFoundException(Long id) {
        this.id = id;
    }
    @Override
    public String toString() {
        if (this.email != null)
            return String.format(" Client with Email : %d  Not Found",email);
        return String.format(" Client with Id : %d  Not Found",id);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
