package tn.insat.persistance.exception;

public class BookExistException extends  Exception {
    private Long id;

    public BookExistException(Long id) {
        this.id = id;
    }


    @Override
    public String toString() {
        return String.format(" Client with Id : %d Already Exist on database",id);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
