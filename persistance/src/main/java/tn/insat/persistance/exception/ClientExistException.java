package tn.insat.persistance.exception;

public class ClientExistException extends Exception {
    private String email;

    public ClientExistException(String email) {
        this.email = email;
    }


    @Override
    public String toString() {
        return String.format(" Client with Email : %d Already Exist on database",email);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
