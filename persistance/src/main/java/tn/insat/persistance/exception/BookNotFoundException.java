package tn.insat.persistance.exception;

public class BookNotFoundException extends Exception {
    private Long id;

    public BookNotFoundException(Long id) {
        this.id = id;
    }


    @Override
    public String toString() {
        return String.format(" Client with Id : %d not found on database",id);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
