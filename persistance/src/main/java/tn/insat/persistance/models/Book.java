package tn.insat.persistance.models;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
public class Book {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long bookId;
    private String bookName;
    private float bookPrice;
    private String description;
    @Column(length=10485760)
    private String bookThumbnail;
    private String authorName;
    private int quantite;

    @JsonIgnore
    @ManyToMany(mappedBy = "books")
    private List<Command> commands;

    public Long getBookId() {
        return bookId;
    }

    public String getBookName() {
        return bookName;
    }

    public float getBookPrice() {
        return bookPrice;
    }

    public String getDescription() {
        return description;
    }

    public String getBookThumbnail() {
        return bookThumbnail;
    }

    public List<Command> getCommands() {
        return commands;
    }

    public String getAuthorName() {
        return authorName;
    }

    public int getQuantite() {
        return quantite;
    }


    public void setBookId(Long bookId) {
        this.bookId = bookId;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public void setBookPrice(float bookPrice) {
        this.bookPrice = bookPrice;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setBookThumbnail(String bookThumbnail) {
        this.bookThumbnail = bookThumbnail;
    }

    public void setCommands(List<Command> commands) {
        this.commands = commands;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    public Book() {
    }

    public Book(String bookName, String authorName) {
        this.bookName = bookName;
        this.authorName = authorName;
    }
    public Book(String bookName, String authorName,int quantite) {
        this.bookName = bookName;
        this.authorName = authorName;
        this.quantite = quantite;
    }
    public Book(String bookName, float bookPrice, String description, String bookThumbnail, String authorName, int quantite, List<Command> commands) {
        this.bookName = bookName;
        this.bookPrice = bookPrice;
        this.description = description;
        this.bookThumbnail = bookThumbnail;
        this.authorName = authorName;
        this.quantite = quantite;
        this.commands = commands;
    }
}
