package tn.insat.persistance.models;

public enum RoleName {
    ROLE_USER,
    ROLE_ADMIN
}
