package tn.insat.persistance.models;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class Command {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long commandId;

    private Date dateCommand;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    private Client client;

    @JsonIgnore
    @ManyToMany
    private List<Book> books;

    private double total;

    public Command(Date dateCommand, Client client, List<Book> books, double total) {
        this.dateCommand = dateCommand;
        this.client = client;
        this.books = books;
        this.total = total;
    }

    public Command() {
    }


    public void setCommandId(Long commandId) {
        this.commandId = commandId;
    }

    public void setDateCommand(Date dateCommand) {
        this.dateCommand = dateCommand;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public Long getCommandId() {
        return commandId;
    }

    public Date getDateCommand() {
        return dateCommand;
    }

    public Client getClient() {
        return client;
    }

    public List<Book> getBooks() {
        return books;
    }

    public double getTotal() {
        return total;
    }

    public Command(Date dateCommand) {
        this.dateCommand = dateCommand;
    }
}
