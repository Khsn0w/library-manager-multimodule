package tn.insat.persistance.models;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Entity
@Table(uniqueConstraints = {
        @UniqueConstraint(columnNames = {
                "email"
        })
})
public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long clientId;

    @Email
    private String email;

    private String clientName;

    private String clientUsername;
    @OneToMany(mappedBy = "client", fetch = FetchType.EAGER)
    private List<Command> commands;


    @NotBlank
    @Size(max = 100)
    @JsonIgnore
    private String password;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "user_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles = new HashSet<>();

    public Long getClientId() {
        return clientId;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getClientUsername() {
        return clientUsername;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public List<Command> getCommands() {
        return commands;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getClientName() {
        return clientName;
    }


    public void setCommands(List<Command> commands) {
        this.commands = commands;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public void setClientUsername(String clientUsername) {
        this.clientUsername = clientUsername;
    }

    public Client(String email, String clientName) {
        this.email = email;
        this.clientName = clientName;
    }

    public Client(String email, String clientName, String clientUsername, String password) {
        this.email = email;
        this.clientName = clientName;
        this.clientUsername = clientUsername;
        this.password = password;
    }
    public Client() {
    }
}
