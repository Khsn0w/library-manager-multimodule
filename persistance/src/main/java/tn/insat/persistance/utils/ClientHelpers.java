package tn.insat.persistance.utils;

import tn.insat.persistance.models.Client;

public class ClientHelpers {
    /**
     *
     * @param c1 Client to Be Updated
     * @param c2 Client To Copy from values
     */
    public static void updateClient(Client c1 , Client c2){
        c1.setEmail(c2.getEmail());
        c1.setClientName(c2.getClientName());
    }
}
