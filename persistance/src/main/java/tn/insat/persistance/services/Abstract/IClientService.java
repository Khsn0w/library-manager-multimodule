package tn.insat.persistance.services.Abstract;

import tn.insat.persistance.exception.ClientExistException;
import tn.insat.persistance.exception.ClientNotFoundException;
import tn.insat.persistance.models.Client;

import java.util.List;

/**
 * Client Crud Service
 */
public interface IClientService {
    void create(Client client) throws ClientExistException;
    void update(Client client) throws ClientNotFoundException;
    Client findById(Long id) throws ClientNotFoundException;
    List<Client> getAll();
    Boolean deleteById(Long id) throws ClientNotFoundException;
    Boolean delete(Client client) throws ClientNotFoundException;
    Client findByEmail(String email) throws ClientNotFoundException;

}
