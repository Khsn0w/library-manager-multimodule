package tn.insat.persistance.services.Abstract;


import tn.insat.persistance.exception.BookNotFoundException;
import tn.insat.persistance.models.Book;

import java.util.List;

/**
 * Book Service For Crud Operations
 */
public interface IBookService {
    void create(Book book);

    void update(Book book) throws BookNotFoundException;

    Book findById(Long id) throws BookNotFoundException;

    List<Book> getAll() ;

    Boolean deleteById(Long id) throws BookNotFoundException;

    Boolean delete(Book book) throws BookNotFoundException;
}
