package tn.insat.persistance.services.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tn.insat.persistance.Repositories.ICommandRepository;
import tn.insat.persistance.models.Command;
import tn.insat.persistance.services.Abstract.ICommandService;

import java.util.List;
import java.util.Optional;
@Service
public class CommandService implements ICommandService {

    @Autowired
    private ICommandRepository commandRepository;

    @Override
    public void create(Command command) {
        commandRepository.save(command);
    }

    @Override
    public void update(Command command) {
        commandRepository.save(command);
    }

    @Override
    public Command findById(Long id) {
        Optional<Command> toReturn = commandRepository.findById(id);
        if (toReturn.isPresent()){
            return toReturn.get();
        }
        return null;
    }

    @Override
    public List<Command> getAll() {
        return commandRepository.findAll();
    }

    @Override
    public Boolean deleteById(Long id) {
        Optional<Command> toReturn = commandRepository.findById(id);
        if (toReturn.isPresent()){
            commandRepository.deleteById(id);
            return true;
        }
        return false;
    }

    @Override
    public Boolean delete(Command command) {
        Optional<Command> toReturn = commandRepository.findById(command.getCommandId());
        if (toReturn.isPresent()){
            commandRepository.deleteById(command.getCommandId());
            return true;
        }
        return false;
    }

    @Override
    public List<Command> findByClient(long id) {

        return commandRepository.findAllByClient(id);

    }
}
