package tn.insat.persistance.services.Abstract;

import tn.insat.persistance.exception.ClientNotFoundException;
import tn.insat.persistance.viewmodels.BuyServiceRequest;


public interface IBuyService {
    void buyBooks(BuyServiceRequest request) throws ClientNotFoundException;
}
