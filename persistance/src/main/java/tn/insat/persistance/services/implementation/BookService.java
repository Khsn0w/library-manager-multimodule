package tn.insat.persistance.services.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tn.insat.persistance.Repositories.IBookRepository;
import tn.insat.persistance.exception.BookNotFoundException;
import tn.insat.persistance.models.Book;
import tn.insat.persistance.services.Abstract.IBookService;

import java.util.List;
import java.util.Optional;

@Service
public class BookService implements IBookService {
    @Autowired
    private IBookRepository bookRepository;

    @Override
    public void create(Book book) {
        bookRepository.save(book);
    }

    @Override
    public void update(Book book) throws BookNotFoundException{
        Optional<Book> toReturn = bookRepository.findById(book.getBookId());
        if (toReturn.isPresent()){
            bookRepository.save(book);
        }else{
            throw new BookNotFoundException(book.getBookId());
        }

    }

    @Override
    public Book findById(Long id) throws BookNotFoundException {
        Optional<Book> toReturn = bookRepository.findById(id);
        if (toReturn.isPresent())
            return toReturn.get();
        throw new BookNotFoundException(id);
    }

    @Override
    public List<Book> getAll() {
        return bookRepository.findAll();
    }

    @Override
    public Boolean deleteById(Long id) throws BookNotFoundException {
        Optional<Book> toReturn = bookRepository.findById(id);
        if (toReturn.isPresent()) {
            bookRepository.deleteById(id);
            return true;
        }
        throw new BookNotFoundException(id);
    }

    @Override
    public Boolean delete(Book book) throws BookNotFoundException {
        Optional<Book> toReturn = bookRepository.findById(book.getBookId());
        if (toReturn.isPresent()) {
            bookRepository.deleteById(book.getBookId());
            return true;
        }
        throw new BookNotFoundException(book.getBookId());
    }
}
