package tn.insat.persistance.services.Abstract;

import tn.insat.persistance.models.Command;

import java.util.List;

/**
 * Service For Command Crud Operations
 */
public interface ICommandService {
    void create(Command command);

    void update(Command command);

    Command findById(Long id);

    List<Command> getAll();

    Boolean deleteById(Long id);

    Boolean delete(Command command);

    List<Command> findByClient(long id);
}
