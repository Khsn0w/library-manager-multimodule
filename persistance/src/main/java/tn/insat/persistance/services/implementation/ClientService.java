package tn.insat.persistance.services.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tn.insat.persistance.Repositories.IClientRepository;
import tn.insat.persistance.exception.ClientExistException;
import tn.insat.persistance.exception.ClientNotFoundException;
import tn.insat.persistance.models.Client;
import tn.insat.persistance.services.Abstract.IClientService;
import tn.insat.persistance.utils.ClientHelpers;

import java.util.List;
import java.util.Optional;

@Service
public class ClientService implements IClientService {
    @Autowired
    private IClientRepository clientRepository;

    @Override
    public void create(Client client) throws ClientExistException {
        Optional<Client> old = clientRepository.findByEmail(client.getEmail());
        if (old.isPresent()) {
            throw new ClientExistException(client.getEmail());
        } else {
            clientRepository.save(client);
        }

    }

    @Override
    public void update(Client client) throws ClientNotFoundException {
        Optional<Client> old = clientRepository.findByEmail(client.getEmail());
        if (!old.isPresent()) {
            throw new ClientNotFoundException(client.getClientId());
        } else {
            Client toUpdate = old.get();
            ClientHelpers.updateClient(toUpdate, client);
            clientRepository.save(toUpdate);
        }
    }

    @Override
    public Client findById(Long id) throws ClientNotFoundException {
        Optional<Client> old = clientRepository.findById(id);
        if (old.isPresent()) {
            return old.get();
        } else {
            throw new ClientNotFoundException(id);
        }
    }

    @Override
    public List<Client> getAll() {
        return clientRepository.findAll();
    }

    @Override
    public Boolean deleteById(Long id) throws ClientNotFoundException {
        Optional<Client> old = clientRepository.findById(id);
        if (old.isPresent()) {
            clientRepository.deleteById(id);
            return true;
        } else {
            throw new ClientNotFoundException(id);
        }
    }

    @Override
    public Boolean delete(Client client) throws ClientNotFoundException {
        Optional<Client> old = clientRepository.findByEmail(client.getEmail());
        if (old.isPresent()) {
            clientRepository.deleteById(client.getClientId());
            return true;
        } else {
            throw new ClientNotFoundException(client.getEmail());
        }
    }

    @Override
    public Client findByEmail(String email) throws ClientNotFoundException {
        Optional<Client> client = clientRepository.findByEmail(email);
        if (client.isPresent())
        {
            return client.get();
        }
        throw new ClientNotFoundException(email);
    }
}
