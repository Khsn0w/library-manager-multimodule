package tn.insat.persistance.services.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tn.insat.persistance.exception.BookNotFoundException;
import tn.insat.persistance.exception.ClientNotFoundException;
import tn.insat.persistance.models.Book;
import tn.insat.persistance.models.Client;
import tn.insat.persistance.models.Command;
import tn.insat.persistance.services.Abstract.IBookService;
import tn.insat.persistance.services.Abstract.IBuyService;
import tn.insat.persistance.services.Abstract.IClientService;
import tn.insat.persistance.services.Abstract.ICommandService;
import tn.insat.persistance.viewmodels.BuyServiceRequest;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class BuyService implements IBuyService {

    @Autowired
    IClientService clientService;
    @Autowired
    IBookService bookService;

    @Autowired
    ICommandService commandService;


    /**
     * Buy Book
     * Store a new command in the database
     *
     * @param request
     */
    @Override
    public void buyBooks(BuyServiceRequest request) throws ClientNotFoundException {
        Date commandDate = new Date();

        Client client = clientService.findById(request.getClientId());

        List<Book> allBooks = bookService.getAll();
        List<Book> books = allBooks
                .stream()
                .filter(a -> request.getBooks().contains(a.getBookId()))
                .collect(Collectors.toList());
        Command newCommand = new Command(commandDate);
        newCommand.setBooks(books);
        newCommand.setClient(client);
        double totalPrice = books.stream().mapToDouble(Book::getBookPrice).sum();
        newCommand.setTotal(totalPrice);
        commandService.create(newCommand);
        for (Book book :
                books) {
            book.setQuantite(book.getQuantite() - 1);
            try{
                bookService.update(book);
            }catch (BookNotFoundException ex){

            }

        }
    }
}
