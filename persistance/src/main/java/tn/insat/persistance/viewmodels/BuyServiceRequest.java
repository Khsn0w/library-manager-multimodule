package tn.insat.persistance.viewmodels;

import java.util.List;

public class BuyServiceRequest {
    private long clientId;
    private List<Long> books;

    public BuyServiceRequest(long clientId, List<Long> books) {
        this.clientId = clientId;
        this.books = books;
    }

    public List<Long> getBooks() {
        return books;
    }

    public long getClientId() {
        return clientId;
    }

    public void setClientId(long clientId) {
        this.clientId = clientId;
    }

    public void setBooks(List<Long> books) {
        this.books = books;
    }
}
