package tn.insat.persistance;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class LibrarymodelsApplication {

    public static void main(String[] args) {
        SpringApplication.run(LibrarymodelsApplication.class, args);
    }

}

