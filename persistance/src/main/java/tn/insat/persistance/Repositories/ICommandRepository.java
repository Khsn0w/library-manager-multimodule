package tn.insat.persistance.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import tn.insat.persistance.models.Command;

import java.util.List;


public interface ICommandRepository extends JpaRepository<Command,Long> {
    List<Command> findAllByClient(long id);
}
