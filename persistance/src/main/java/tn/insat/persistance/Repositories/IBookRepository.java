package tn.insat.persistance.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import tn.insat.persistance.models.Book;


public interface IBookRepository extends JpaRepository<Book, Long> {
}
