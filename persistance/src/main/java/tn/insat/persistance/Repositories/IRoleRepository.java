package tn.insat.persistance.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import tn.insat.persistance.models.Role;
import tn.insat.persistance.models.RoleName;

import java.util.Optional;


public interface IRoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(RoleName roleName);
}
