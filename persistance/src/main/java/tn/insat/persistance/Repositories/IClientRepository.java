package tn.insat.persistance.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import tn.insat.persistance.models.Client;

import java.util.Optional;



public interface IClientRepository extends JpaRepository<Client,Long> {
    Optional<Client> findByEmail(String email);
}
