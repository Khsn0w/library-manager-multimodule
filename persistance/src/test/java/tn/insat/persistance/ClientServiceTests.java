package tn.insat.persistance;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.CleanupFailureDataAccessException;
import org.springframework.test.context.junit4.SpringRunner;
import tn.insat.persistance.exception.ClientExistException;
import tn.insat.persistance.exception.ClientNotFoundException;
import tn.insat.persistance.models.Client;
import tn.insat.persistance.services.Abstract.IClientService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.util.List;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ClientServiceTests {
    @Autowired
    IClientService clientService;

    @Test
    public void testget() throws ClientExistException {
        Client client = new Client("souhaiel_riahi_initial@hotmail.fr", "Souhaiel Riahi", "khsn0w", "password");
        try{
            clientService.create(client);
            List<Client> clients = clientService.getAll()
                    .stream()
                    .filter(c -> c.getEmail().equals(client.getEmail()))
                    .collect(Collectors.toList());
            assertEquals(1,clients.size());
        }catch(ClientExistException exception){
            throw new ClientExistException(client.getEmail());
        }
    }



    @Test
    public void teststore() throws ClientExistException {
        Client client = new Client("souhaiel_riahi@hotmail.fr", "Souhaiel Riahi", "khsn0w", "password");
        try {
            clientService.create(client);
            Client stored = clientService.findByEmail("souhaiel_riahi@hotmail.fr");
            assertEquals(stored.getClientName(),"Souhaiel Riahi");
        } catch (ClientExistException | ClientNotFoundException exception) {
            throw new ClientExistException(client.getEmail());
        }
    }

    @Test(expected = ClientExistException.class)
    public void teststoresameclient() throws ClientExistException {
        Client client = new Client("souhaiel_riahi_duplicate@hotmail.fr", "Souhaiel Riahi", "khsn0w", "password");
        Client duplicatedClient = new Client("souhaiel_riahi_duplicate@hotmail.fr", "Souhaiel Riahi", "khsn0w", "password");
        try {
            clientService.create(client);
            clientService.create(duplicatedClient);
        } catch (ClientExistException exception) {
            throw new ClientExistException(duplicatedClient.getEmail());
        }
    }

    @Test
    public void testupdate() throws ClientNotFoundException{
        Client toStore = new Client("test_update@gmail.com", "TEST UPDATE" ,"Khsn0w" ,"password" );
        try{
            clientService.create(toStore);
            Client toUpdate = clientService.findByEmail("test_update@gmail.com");
            toUpdate.setClientName("UPDATED");
            clientService.update(toUpdate);
            Client client = clientService.findByEmail("test_update@gmail.com");
            assertEquals("UPDATED",client.getClientName());
        }catch(ClientExistException exception){

        }
    }

    @Test(expected = ClientNotFoundException.class)
    public void testupdatewithnoclient() throws ClientNotFoundException {
        Client toStore = new Client("test_update_duplicate@gmail.com", "TEST UPDATE" ,"Khsn0w" ,"password" );
        try{

            clientService.update(toStore);
        }catch(ClientNotFoundException exception){
            throw new ClientNotFoundException(toStore.getEmail());
        }
    }

    @Test
    public void testdelete() throws ClientNotFoundException{
        Client toDelete = new Client("test_delete@gmail.com", "TEST UPDATE" ,"Khsn0w" ,"password" );

        try{
            clientService.create(toDelete);

            clientService.delete(toDelete);

            List<Client> clients = clientService.getAll().stream()
                    .filter(c -> c.getEmail().equals(toDelete.getEmail()))
                    .collect(Collectors.toList());
            assertEquals(0 ,clients.size());
        }catch
        (ClientNotFoundException | ClientExistException exception){
            throw new ClientNotFoundException(toDelete.getEmail());
        }
    }

    @Test(expected = ClientNotFoundException.class)
    public void testdeletewithnoclient() throws ClientNotFoundException{
        Client toDelete = new Client("test_delete_no@gmail.com", "TEST UPDATE" ,"Khsn0w" ,"password" );
        try{
            clientService.delete(toDelete);
        }catch
        (ClientNotFoundException exception){
            throw new ClientNotFoundException(toDelete.getEmail());
        }
    }

}

