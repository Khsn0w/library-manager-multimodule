package tn.insat.persistance;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import tn.insat.persistance.exception.ClientExistException;
import tn.insat.persistance.exception.ClientNotFoundException;
import tn.insat.persistance.models.Book;
import tn.insat.persistance.models.Client;
import tn.insat.persistance.services.Abstract.IBookService;
import tn.insat.persistance.services.Abstract.IBuyService;
import tn.insat.persistance.services.Abstract.IClientService;
import tn.insat.persistance.viewmodels.BuyServiceRequest;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BuyServiceTests {

    @Autowired
    IBuyService buyService;

    @Autowired
    IClientService clientService;

    @Autowired
    IBookService bookService;


    /**
     * Initialisation for test Purpose
     */
    @Before
    public  void setUp() throws ClientExistException {
        try{
            clientService.findByEmail("souhaiel_riahi_buy@gmail.com");
        }catch(ClientNotFoundException exception){
            Client client = new Client("souhaiel_riahi_buy@gmail.com", "Souhaiel Commands");
            List<Book> books = Arrays.asList(
                    new Book("SPRING BOOT", "SOUHAIEL",10),
                    new Book("HIBERNATE", "M ROMDHA     NI",10)
            );
            clientService.create(client);
            for (Book book :
                    books) {
                bookService.create(book);
            }
        }
    }


    @Test
    public void testbuy() throws ClientNotFoundException{
        /**
         * Prepare Request As Described
         */
       Client client = clientService.findByEmail("souhaiel_riahi_buy@gmail.com");
       List<Long> books = bookService.getAll()
               .stream()
               .map(c -> c.getBookId())
               .collect(Collectors.toList());
        BuyServiceRequest request = new BuyServiceRequest(client.getClientId(),books);
        int oldQuantity = bookService.getAll()
                .stream()
                .mapToInt(c -> c.getQuantite())
                .sum();

        buyService.buyBooks(request);
        List<Book> updatebooks = bookService.getAll();

        int newQuantity = oldQuantity - books.size();
        int calculatedQuantiy = updatebooks.stream()
                .mapToInt(c -> c.getQuantite())
                .sum();
        assertEquals(newQuantity,calculatedQuantiy);
    }

}
