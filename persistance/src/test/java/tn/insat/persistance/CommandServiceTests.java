package tn.insat.persistance;

import org.aspectj.apache.bcel.generic.InstructionCLV;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.jupiter.api.BeforeAll;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import tn.insat.persistance.exception.ClientExistException;
import tn.insat.persistance.exception.ClientNotFoundException;
import tn.insat.persistance.models.Book;
import tn.insat.persistance.models.Client;
import tn.insat.persistance.models.Command;
import tn.insat.persistance.services.Abstract.IBookService;
import tn.insat.persistance.services.Abstract.IClientService;
import tn.insat.persistance.services.Abstract.ICommandService;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CommandServiceTests {

    @Autowired
    ICommandService commandService;

    @Autowired
    IClientService clientService;

    @Autowired
    IBookService bookService;


    /**
     * Initialisation for test Purpose
     */
    @Before
    public  void setUp() throws ClientExistException {
        try{
            clientService.findByEmail("souhaiel_riahi_command@gmail.com");
        }catch(ClientNotFoundException exception){
            Client client = new Client("souhaiel_riahi_command@gmail.com", "Souhaiel Commands");
            List<Book> books = Arrays.asList(
                    new Book("SPRING BOOT", "SOUHAIEL"),
                    new Book("HIBERNATE", "M ROMDHANI")
            );
            clientService.create(client);
            for (Book book :
                    books) {
                bookService.create(book);
            }
        }
    }


    /**
     * Test Create
     */

    @Test
    public void testcreate() throws ClientNotFoundException {

        Client client = clientService.findByEmail("souhaiel_riahi_command@gmail.com");
        List<Book> books = bookService.getAll();
        double total = books.stream().mapToDouble(c -> c.getBookPrice()).sum();
        Command command = new Command(new Date(), client, books, total);
        commandService.create(command);

        Command stored = commandService.findById(command.getCommandId());
        assert Math.abs(stored.getTotal() - total) <= 0.001;
    }

    /**
     * Test Update Command
     */

    /**
     * Test delete Commands
     */

    @Test
    public void deleteCommand() throws ClientNotFoundException {
        Client client = clientService.findByEmail("souhaiel_riahi_command@gmail.com");
        List<Book> books = bookService.getAll();
        double total = books.stream().mapToDouble(c -> c.getBookPrice()).sum();
        Command command = new Command(new Date(), client, books, total);
        commandService.create(command);

        Command stored = commandService.findById(command.getCommandId());
        Boolean deleted = commandService.deleteById(stored.getCommandId());
        assertEquals(deleted, true);
    }
}
