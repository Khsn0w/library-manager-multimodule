package tn.insat.persistance;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import tn.insat.persistance.exception.BookNotFoundException;
import tn.insat.persistance.models.Book;
import tn.insat.persistance.models.Client;
import tn.insat.persistance.services.Abstract.IBookService;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BookServiceTests {

    @Autowired
    IBookService bookService;

    /**
     * Store Test
     */

    @Test
    public void teststore() throws BookNotFoundException {
        Book book = new Book("Spring Boot", "Souhaiel");
        bookService.create(book);
        try {
            Book stored = bookService.findById(book.getBookId());
        } catch (BookNotFoundException exception) {
            throw new BookNotFoundException(book.getBookId());
        }
    }

    /**
     * Update Test
     */

    @Test
    public void testupdate() throws BookNotFoundException {
        Book book = new Book("SPRING TEST", "M ROMDHANI");
        bookService.create(book);

        try {
            book.setQuantite(5);
            bookService.update(book);
            Book updated = bookService.findById(book.getBookId());
            assertEquals(5, updated.getQuantite());
        } catch (BookNotFoundException exception) {
            throw exception;
        }
    }

    @Test(expected = BookNotFoundException.class)
    public void testupdatewithoutbook() throws BookNotFoundException {
        Book book = new Book("BOOKFAIL", "FAAIL");
        book.setBookId(Long.MAX_VALUE);
        try {
            bookService.update(book);
        } catch (BookNotFoundException exception) {
            throw exception;
        }
    }

    /**
     * Delete Test
     */

    @Test
    public void testdelete() throws BookNotFoundException {
        Book book = new Book("SPRING TEST DELETE", "M ROMDHANI");
        bookService.create(book);
        try {
            bookService.delete(book);
            List<Book> books = bookService.getAll().stream().filter(b -> b.getBookName().equals("SPRING TEST DELETE"))
                    .collect(Collectors.toList());
            assertEquals(0, books.size());
        } catch (BookNotFoundException exception) {
            throw exception;
        }
    }

    @Test(expected = BookNotFoundException.class)
    public void testdeletewithnobook() throws BookNotFoundException {
        Book book = new Book("SPRING TEST DELETE NO EXISTING", "M ROMDHANI");
        book.setBookId(Long.MAX_VALUE);
        try {
            bookService.delete(book);
            List<Book> books = bookService.getAll().stream().filter(b -> b.getBookName().equals("SPRING TEST DELETE NO EXISTING"))
                    .collect(Collectors.toList());
            assertEquals(0, books.size());
        } catch (BookNotFoundException exception) {
            throw exception;
        }
    }

    @Test
    public void deleteById () throws BookNotFoundException{
        Book book = new Book("SPRING TEST DELETE BY ID", "M ROMDHANI");
        bookService.create(book);
        try {
            bookService.deleteById(book.getBookId());
            List<Book> books = bookService.getAll().stream().filter(b -> b.getBookName().equals("SPRING TEST DELETE BY ID"))
                    .collect(Collectors.toList());
            assertEquals(0, books.size());
        } catch (BookNotFoundException exception) {
            throw exception;
        }
    }


}
